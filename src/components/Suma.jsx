import React from 'react'
import { useRef,useState } from 'react'
import { Container,Row,Col,Input,Form,Button } from 'reactstrap'
import Swal from 'sweetalert2';
const Suma = () => {
    //hook de estado
    const [suma,setSuma]=useState(0);
    const [n1,setN1] = useState('');
    const [n2,setN2] = useState('');
    const sumar =() => {
        //funcion sumar
        setSuma(parseInt(n1)+parseInt(n2))
        Swal.fire(
            'El resultado es : '+suma ,
            '',
            'success'
        )
    }
  return (
    <Container>
        <Row className='mt-3'>
            <Col>
                <Form>
                    <Row>
                        <Col md='4'>
                            <Input onChange={(e) => {setN1(e.target.value)}} placeholder='Número 1' type='number'> </Input>
                        </Col>
                        <Col md='4'>
                            <Input onChange={(e) => {setN2(e.target.value)}} placeholder='Número 2' type='number'> </Input>
                        </Col>
                        <Col md='4'>
                            <Button onClick={sumar} color='dark'>Sumar</Button>
                        </Col>
                    </Row>
                </Form>
            </Col>
        </Row>
       Suma : {suma}
    </Container>
  )
}

export default Suma