import React from 'react'
import { Nav,NavItem,NavLink } from 'reactstrap'

const BarraNavegacion = () => {
  return (
    <Nav color='dark' className='bg-info' >
  <NavItem>
    <NavLink
      active
      href="/suma"
    >
      Suma
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink href="/tabla">
      Tabla
    </NavLink>
  </NavItem>
  <NavItem>
    <NavLink
      
      href="/arrays"
    >
      Array
    </NavLink>
  </NavItem>
</Nav>
  )
}

export default BarraNavegacion