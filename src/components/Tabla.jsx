import React from 'react'
import { Container,Row,Col,Table,Input,Form,Button,InputGroup,InputGroupText } from 'reactstrap'
import { useRef,useState } from 'react'


const Tabla = () => {
  const [colores,setColores] = useState ([])
  const add = (e,color) =>{
    if(color.trim()!='' && e.charCode ==13){
      setColores(colores => colores.concat(color));
      e.target.value = '';
    }
  }  
  return (
    <Container>
        <Row className='mt-3'>
          <Col md='6'>
            <InputGroup>
              <InputGroupText><i className='fa-solid fa-palette'></i></InputGroupText>
              <Input placeholder='Color' onKeyPress={(e)=>{add(e,e.target.value)}}/>
            </InputGroup> 
          </Col>
          <Col md='6'>
            <Table responsive>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Color</th>
                </tr>
              </thead>
              <tbody>
                {colores.map((c,i) => (
                  <tr key={i}>
                    <td>{i+1}</td><td>{c}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
    </Container>
  )
}

export default Tabla