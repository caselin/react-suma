import { useState } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Nav from './components/Nav'
import Suma from './components/suma'
import Tabla from './components/Tabla'
import Arrays from './components/Arrays'
function App() {

  return (
    <>
    <BrowserRouter>
      <Nav></Nav>
      <Routes>
        <Route path='/suma'element={<Suma />} />
        <Route path='/tabla'element={<Tabla />} />
        <Route path='/arrays'element={<Arrays />} />
      </Routes>
    </BrowserRouter>
    </>
  )
}

export default App
